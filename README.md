# REDAXO mit Gulp, Browserify, PostCSS, Pug, EDgrid und Bimmelbam 2

## Installation global requeriments
- Npm
- Docker
- Docker Compose
- Gulp

## Project setup
```
cp .env.example .env
```
```
npm install
```
```
docker-compose up -d
```

### Compiles assets and hot-reloads for development
```
gulp
```

## Backups of database and assets