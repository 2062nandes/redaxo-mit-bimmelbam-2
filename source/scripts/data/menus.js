export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Item 1',
    href: '#'
  },
  {
    title: 'Item 2',
    href: '#'
  },
  {
    title: 'Item 3',
    href: '#'
  },
  {
    title: 'Item 4',
    href: '#'
  },
  {
    title: 'Item 5',
    href: '#'
  }
]
